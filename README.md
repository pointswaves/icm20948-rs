# ICM20948

A platform and bus agnostic Rust driver for the TDK ICM20948, based on the embedded-hal traits.

## Documentation

Currently the docs can be found at https://pointswaves.gitlab.io/icm20948-rs/icm20948/index.html

## Current structure

The lib contains code for the ICM and code to wrap I2C and SPI buses in structs to effectively mange byte based registers

The code to abstract the buses and provide boiler plate for accessing sub byte structures with in bytes based registers is
being developed along side the ICM code but will be moved to its own crate once it is more mature.

## Library Aims

The library uses the [BitByteStructs](https://gitlab.com/pointswaves/bit-byte-structures) for a lot of the boiler plate
when reading and writing to the ICM20948s registers. This lib is currently also acting as a example of how to use BitByteStruts
but if a better way to mange them comes up then this create may swap over.

The library currently work to retrieve the Acceleration and Gyro data from the ICM over I2C but the library aims
to provide this data and the magnetic data over I2C or SPI.

Be published to crates.io
