#![no_std]

//! A platform agnostic Rust driver for the TDK ICM20948, based on the
//! [`embedded-hal`](https://github.com/japaric/embedded-hal) traits.
//!
//! ## The Device
//!
//! The device is a MEMS based 9 degree of freedom ICM
//! The IC contains 3 axis accelerometers, gyroscopes and manometers
//! The IC can comunicate over a SPI or I2C bus
//!
//! ## The Library
//!
//! Contains both I2C and SPI implemntations both sharing common code
//!
//! ## Very basic example
//!
//! this might work on a pi or something similar
//!
//! ```no_run
//! extern crate icm20948;
//! extern crate linux_embedded_hal as hal;
//!
//! use hal::spidev::{self, SpidevOptions};
//! use hal::{Pin, Spidev};
//! use hal::sysfs_gpio::Direction;
//! use hal::Delay;
//!
//! const ICM_CS_PIN: u64 = 8;
//!
//! fn main(){
//!
//!     let mut spi = Spidev::open("/dev/spidev0.0").unwrap();
//!     let options = SpidevOptions::new()
//!         .bits_per_word(8)
//!         .max_speed_hz(20_000)
//!         .build();
//!     spi.configure(&options).unwrap();
//!
//!     let mut cs = Pin::new(ICM_CS_PIN);
//!     cs.export().unwrap();
//!     cs.set_direction(Direction::Out).unwrap();
//!
//!     let mut icm_obj = icm20948::ICMSPI::new(
//!         &mut spi, &mut cs)
//!         .expect("Failed to communicate with icm module!");
//!
//!     icm_obj.init(&mut spi, &mut cs, &mut Delay);
//!
//!     let (xa, ya, za, xg, yg, zg) =
//!            icm_obj.scale_raw_accel_gyro(icm_obj.get_values_accel_gyro(&mut spi, &mut cs).unwrap());
//!     //println!("Sensed, {:?}, {:?}, {:?}, {:?}, {:?}, {:?}", xa, ya, za, xg, yg, zg);
//! }
//! ```

const ICM20X_B0_WHOAMI: u8 = 0x00;
/// ICM20948 default device id from WHOAMI
const ICM20948_CHIP_ID: u8 = 0xEA;
/// Default ICM20948 device address
pub const ICM20948_CHIP_ADR: u8 = 0x69;
/// Alternative ICM20948 device address
pub const ICM20948_CHIP_ADR_ALT: u8 = 0x68;
const ICM20X_B0_REG_BANK_SEL: u8 = 0x7f;
const ICM20X_B0_LP_CONFIG: u8 = 0x05;
const ICM20X_B0_PWR_MGMT_1: u8 = 0x06;
const ICM20X_B0_PWR_MGMT_2: u8 = 0x07;
/// First byte of the accel data
const ICM20X_B0_ACCEL_XOUT_H: u8 = 0x2D;
const _ICM20X_B0_GYRO_XOUT_H: u8 = 0x33;
const ICM20X_B2_GYRO_SMPLRT_DIV_1: u8 = 0x00;
const ICM20X_B2_GYRO_CONFIG: u8 = 0x01;
const ICM20X_B2_GYRO_CONFIG_2: u8 = 0x02;
const ICM20X_B2_ACCEL_SMPLRT_DIV_1: u8 = 0x10;
const ICM20X_B2_ACCEL_CONFIG: u8 = 0x14;
const ICM20X_B2_ACCEL_CONFIG_2: u8 = 0x15;
const _ICM20X_B0_TEMP_OUT_H: u8 = 0x39;

const ICM20X_B0_REG_INT_PIN_CFG: u8 = 0x0F;
const ICM20X_B3_I2C_MST_CTRL: u8 = 0x01;
const ICM20X_B3_I2C_MST_DELAY_CTRL: u8 = 0x02;

const GRAVECONST: f32 = 9.81;

use core::fmt::Debug;
use core::marker::PhantomData;
#[cfg(feature = "cortex-m-debuging")]
use cortex_m_semihosting::hprintln;
use embedded_hal::blocking::delay::DelayMs;
use embedded_hal::blocking::i2c::{Read, Write, WriteRead};
use embedded_hal::blocking::spi::Transfer;
use embedded_hal::digital::v2::OutputPin;

use bit_byte_structs::registers::{
    BitByteStructError, BitStruct, ByteStructI16, CrossByteBitStructI16,
};

use bit_byte_structs::bus::{self, Interface, InterfaceError};

/// Enum used to report errors when using the ICM
///
/// The Enum can report errors that are caused by the ICM libaray its self
/// But can also be used to pass on errors caused by the underling registers or buses
#[derive(Debug)]
pub enum ICMError<E> {
    /// Pass on Error caused by BitByteStructure
    ByteBitReg(BitByteStructError<InterfaceError<E>>),
    /// Pass on Error caused when using the Bus Interface
    Interface(InterfaceError<E>),
    /// Pass on Error caused when using the Bus its self
    ///
    /// Its will be less common for the ICM code to interact with the bus
    /// directly as more code is ported to the BitByteStructure but some
    /// operations may always be done directly, especially code were
    /// performance is important.
    Raw(E),
    /// Stop if the chip at the spesified address is not reporting the corrcet
    /// self identification code.
    ///
    /// For I2C this is most likely if the ID change jumper is in the wrong state or there is
    /// anther chip on the bus with this address.
    ///
    /// For SPI the CS pins may have been mixed up.
    BadChip,
    /// Returned if the register bank is set to a invalid value
    ///
    /// There are 4 banks, 0,1,2 and 3
    BankOutOfRange,
}
impl<E> From<BitByteStructError<InterfaceError<E>>> for ICMError<E> {
    fn from(err: BitByteStructError<InterfaceError<E>>) -> Self {
        ICMError::ByteBitReg(err)
    }
}
impl<E> From<InterfaceError<E>> for ICMError<E> {
    fn from(err: InterfaceError<E>) -> Self {
        ICMError::Interface(err)
    }
}
impl<E> From<E> for ICMError<E> {
    fn from(err: E) -> Self {
        ICMError::Raw(err)
    }
}

/// Enum describing possible ranges of the Accelerometers
pub enum AccelRangeOptions {
    /// +/- 2G
    G2,
    /// +/- 4G
    G4,
    /// +/- 8G
    G8,
    /// +/- 16G
    G16,
}

impl AccelRangeOptions {
    fn as_float(&self) -> f32 {
        // Values taken from table 2 of the data sheet
        match self {
            &AccelRangeOptions::G2 => 16_384.0,
            &AccelRangeOptions::G4 => 8_192.0,
            &AccelRangeOptions::G8 => 4_096.0,
            &AccelRangeOptions::G16 => 2_048.0,
        }
    }
}

/// Enum describing possible ranges of the Gyros
pub enum GyroRangeOptions {
    /// +/- 250 deg/sec
    Deg250,
    /// +/- 500 deg/sec
    Deg500,
    /// +/- 1000 deg/sec
    Deg1000,
    /// +/- 2000 deg/sec
    Deg2000,
}

impl GyroRangeOptions {
    fn as_float(&self) -> f32 {
        match self {
            &GyroRangeOptions::Deg250 => 131.0,
            &GyroRangeOptions::Deg500 => 65.5,
            &GyroRangeOptions::Deg1000 => 32.8,
            &GyroRangeOptions::Deg2000 => 16.4,
        }
    }
}

/// This struct holds and implements the functions that are bus independent for the ICM20948
///
/// The common code is generally the slow and steady bits, like setting up the configureation registers
/// The fast as posible retreave the data may well end up being bus spesific
pub struct ICMCommon<InterfaceThing: ?Sized, E> {
    phantom: PhantomData<InterfaceThing>,
    sleep_bit: BitStruct<dyn Interface<Error = InterfaceError<E>>, ICM20X_B0_PWR_MGMT_1, 1, 6>,
    chip_id: BitStruct<dyn Interface<Error = InterfaceError<E>>, ICM20X_B0_WHOAMI, 8, 0>,
    bank_select: BitStruct<dyn Interface<Error = InterfaceError<E>>, ICM20X_B0_REG_BANK_SEL, 2, 4>,
    device_reset: BitStruct<dyn Interface<Error = InterfaceError<E>>, ICM20X_B0_PWR_MGMT_1, 1, 7>,
    low_power_bit: BitStruct<dyn Interface<Error = InterfaceError<E>>, ICM20X_B0_PWR_MGMT_1, 1, 5>,
    duty_cycle: BitStruct<dyn Interface<Error = InterfaceError<E>>, ICM20X_B0_LP_CONFIG, 8, 0>,
    accel_range: BitStruct<dyn Interface<Error = InterfaceError<E>>, ICM20X_B2_ACCEL_CONFIG, 2, 1>,
    accel_range_cache: AccelRangeOptions,
    accel_div: CrossByteBitStructI16<dyn Interface<Error = InterfaceError<E>>>,
    accel_lp_fact:
        BitStruct<dyn Interface<Error = InterfaceError<E>>, ICM20X_B2_ACCEL_CONFIG_2, 2, 0>,
    accel_lp_rate:
        BitStruct<dyn Interface<Error = InterfaceError<E>>, ICM20X_B2_ACCEL_CONFIG, 3, 3>,
    accel_lp_en: BitStruct<dyn Interface<Error = InterfaceError<E>>, ICM20X_B2_ACCEL_CONFIG, 1, 0>,
    accel_lp_dlpfcfg:
        BitStruct<dyn Interface<Error = InterfaceError<E>>, ICM20X_B2_ACCEL_CONFIG, 3, 3>,
    gyro_range: BitStruct<dyn Interface<Error = InterfaceError<E>>, ICM20X_B2_GYRO_CONFIG, 2, 1>,
    gyro_range_cache: GyroRangeOptions,
    gyro_div:
        BitStruct<dyn Interface<Error = InterfaceError<E>>, ICM20X_B2_GYRO_SMPLRT_DIV_1, 8, 0>,
    gyro_lp_fact:
        BitStruct<dyn Interface<Error = InterfaceError<E>>, ICM20X_B2_GYRO_CONFIG_2, 2, 0>,
    result_registers: ByteStructI16<dyn Interface<Error = InterfaceError<E>>, 6>,
}

impl<I, E> ICMCommon<I, E>
where
    I: Interface,
    I: ?Sized,
{
    /// Try to create new instance of the ICMCommon struct
    ///
    /// This just sets up all the memory, it dose not try to communicate with the chip yet
    pub fn new() -> Result<Self, BitByteStructError<InterfaceError<E>>> {
        let sleep_bit =
            BitStruct::<dyn Interface<Error = InterfaceError<E>>, ICM20X_B0_PWR_MGMT_1, 1, 6>::new(
            )?;
        let low_power_bit =
            BitStruct::<dyn Interface<Error = InterfaceError<E>>, ICM20X_B0_PWR_MGMT_1, 1, 5>::new(
            )?;
        let chip_id =
            BitStruct::<dyn Interface<Error = InterfaceError<E>>, ICM20X_B0_WHOAMI, 8, 0>::new()?;
        let bank_select = BitStruct::<
            dyn Interface<Error = InterfaceError<E>>,
            ICM20X_B0_REG_BANK_SEL,
            2,
            4,
        >::new()?;
        let device_reset =
            BitStruct::<dyn Interface<Error = InterfaceError<E>>, ICM20X_B0_PWR_MGMT_1, 1, 7>::new(
            )?;
        let duty_cycle =
            BitStruct::<dyn Interface<Error = InterfaceError<E>>, ICM20X_B0_LP_CONFIG, 8, 0>::new(
            )?;
        let accel_range = BitStruct::<
            dyn Interface<Error = InterfaceError<E>>,
            ICM20X_B2_ACCEL_CONFIG,
            2,
            1,
        >::new()?;
        let accel_div = CrossByteBitStructI16::<dyn Interface<Error = InterfaceError<E>>>::new(
            ICM20X_B2_ACCEL_SMPLRT_DIV_1,
            12,
            true,
        )?;
        let accel_lp_fact = BitStruct::<
            dyn Interface<Error = InterfaceError<E>>,
            ICM20X_B2_ACCEL_CONFIG_2,
            2,
            0,
        >::new()?;
        let accel_lp_rate = BitStruct::<
            dyn Interface<Error = InterfaceError<E>>,
            ICM20X_B2_ACCEL_CONFIG,
            3,
            3,
        >::new()?;
        let accel_lp_en = BitStruct::<
            dyn Interface<Error = InterfaceError<E>>,
            ICM20X_B2_ACCEL_CONFIG,
            1,
            0,
        >::new()?;
        let accel_lp_dlpfcfg = BitStruct::<
            dyn Interface<Error = InterfaceError<E>>,
            ICM20X_B2_ACCEL_CONFIG,
            3,
            3,
        >::new()?;
        let gyro_range = BitStruct::<
            dyn Interface<Error = InterfaceError<E>>,
            ICM20X_B2_GYRO_CONFIG,
            2,
            1,
        >::new()?;
        let gyro_div = BitStruct::<
            dyn Interface<Error = InterfaceError<E>>,
            ICM20X_B2_GYRO_SMPLRT_DIV_1,
            8,
            0,
        >::new()?;
        let gyro_lp_fact = BitStruct::<
            dyn Interface<Error = InterfaceError<E>>,
            ICM20X_B2_GYRO_CONFIG_2,
            2,
            0,
        >::new()?;

        let result_registers = ByteStructI16::<dyn Interface<Error = InterfaceError<E>>, 6>::new(
            ICM20X_B0_ACCEL_XOUT_H,
            true,
        )?;

        Ok(ICMCommon::<I, E> {
            phantom: PhantomData,
            sleep_bit,
            chip_id,
            bank_select,
            device_reset,
            low_power_bit,
            duty_cycle,
            accel_range,
            accel_range_cache: AccelRangeOptions::G4,
            accel_div,
            accel_lp_fact,
            accel_lp_rate,
            accel_lp_en,
            accel_lp_dlpfcfg,
            gyro_range,
            gyro_range_cache: GyroRangeOptions::Deg250,
            gyro_div,
            gyro_lp_fact,
            result_registers,
        })
    }

    /// Wake the chip from sleep mode
    pub fn wake(
        &self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
        delay: &mut dyn DelayMs<u16>,
    ) -> Result<(), ICMError<E>> {
        self.set_bank(interface, 0)?;
        self.sleep_bit.write(interface, 0)?;
        delay.delay_ms(20);
        Ok(())
    }

    /// This checks if the chip reports the correct chipID
    pub fn check_chip(
        &self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
    ) -> Result<(), ICMError<E>> {
        self.set_bank(interface, 0)?;
        let chip_id = self.chip_id.read(interface)?;
        let good_id = ICM20948_CHIP_ID;
        #[cfg(feature = "cortex-m-debuging")]
        hprintln!(
            "Chip ID: {:#x} {:#x}, Ok {:?}",
            chip_id,
            good_id,
            good_id == chip_id
        )
        .unwrap();
        if !(good_id == chip_id) {
            return Err(ICMError::BadChip);
        }
        Ok(())
    }

    /// Set the active register bank
    ///
    /// There are 4 register banks, 0, 1, 2 and 3
    pub fn set_bank(
        &self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
        bank: u8,
    ) -> Result<(), ICMError<E>> {
        if bank > 3 {
            return Err(ICMError::BankOutOfRange);
        }
        self.bank_select.write(interface, bank)?;
        Ok(())
    }

    /// Restart the device
    pub fn restart(
        &self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
        delay: &mut dyn DelayMs<u16>,
    ) -> Result<(), ICMError<E>> {
        self.set_bank(interface, 0)?;
        self.device_reset.write(interface, 1)?;
        loop {
            delay.delay_ms(100);
            if self.device_reset.read(interface)? == 0 {
                break;
            }
        }
        Ok(())
    }

    /// Set low power mode
    pub fn set_low_power(
        &self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
        low_power: bool,
    ) -> Result<(), ICMError<E>> {
        self.set_bank(interface, 0)?;
        if low_power {
            self.low_power_bit.write(interface, 1)?;
        } else {
            self.low_power_bit.write(interface, 0)?;
        }
        Ok(())
    }

    /// Turn on low power duty cycles
    pub fn set_low_power_duty_cycle(
        &self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
        duty_cycle: bool,
    ) -> Result<(), ICMError<E>> {
        self.set_bank(interface, 0)?;
        let old_val = self.duty_cycle.read(interface)?;

        let new_value = if duty_cycle {
            old_val & 0b10001111 | 0b11 << 4
        } else {
            old_val & 0b10001111
        };
        self.duty_cycle.write(interface, new_value)?;

        let result = self.duty_cycle.read(interface)?;
        #[cfg(feature = "cortex-m-debuging")]
        hprintln!("duty cycles: {:#09b}", result).unwrap();
        Ok(())
    }

    /// Set the range of the Accelerometers
    pub fn set_accel_range(
        &mut self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
        delay: &mut dyn DelayMs<u16>,
        range: AccelRangeOptions,
    ) -> Result<(), ICMError<E>> {
        self.set_bank(interface, 2)?;
        let raw: u8 = match range {
            AccelRangeOptions::G2 => 0x0,
            AccelRangeOptions::G4 => 0x1,
            AccelRangeOptions::G8 => 0x2,
            AccelRangeOptions::G16 => 0x3,
        };
        self.accel_range.write(interface, raw)?;
        self.accel_range_cache = range;
        Ok(())
    }

    /// Set the rate of sampling of the Accelerometers
    pub fn set_accel_div(
        &self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
        range: u16,
    ) -> Result<(), ICMError<E>> {
        self.set_bank(interface, 2)?;
        self.accel_div.write(interface, range)?;
        Ok(())
    }

    /// Set the sample avarageing of the LP of the Accelerometers
    pub fn set_accel_lp_factor(
        &self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
        rate: u8,
        // Todo u8 to enum
    ) -> Result<(), ICMError<E>> {
        self.set_bank(interface, 2)?;
        self.accel_lp_fact.write(interface, rate)?;
        Ok(())
    }

    /// Set the bandwidth of the LP filter on the Accelerometers
    pub fn set_accel_lp_rate(
        &self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
        rate: u8,
        // Todo u8 to enum
    ) -> Result<(), ICMError<E>> {
        self.set_bank(interface, 2)?;
        self.accel_lp_rate.write(interface, rate)?;
        Ok(())
    }

    /// Set the bandwidth of the LP filter on the Accelerometers
    pub fn set_accel_lp_enable(
        &self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
        rate: u8,
        // Todo u8 to enum
    ) -> Result<(), ICMError<E>> {
        self.set_bank(interface, 2)?;
        self.accel_lp_en.write(interface, rate)?;
        Ok(())
    }

    /// Set the bandwidth of the LP filter on the Accelerometers
    pub fn set_accel_lp_dlpfcfg(
        &self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
        value: u8,
    ) -> Result<(), ICMError<E>> {
        self.set_bank(interface, 2)?;
        self.accel_lp_dlpfcfg.write(interface, value)?;
        Ok(())
    }

    /// Set the range of the Gyros
    pub fn set_gyro_range(
        &mut self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
        range: GyroRangeOptions,
    ) -> Result<(), ICMError<E>> {
        self.set_bank(interface, 2)?;
        let raw: u8 = match range {
            GyroRangeOptions::Deg250 => 0,
            GyroRangeOptions::Deg500 => 1,
            GyroRangeOptions::Deg1000 => 2,
            GyroRangeOptions::Deg2000 => 3,
        };
        self.gyro_range.write(interface, raw)?;
        self.gyro_range_cache = range;
        Ok(())
    }

    /// Set the rate of sampling of the Gyros
    pub fn set_gyro_div(
        &self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
        range: u8,
    ) -> Result<(), ICMError<E>> {
        self.set_bank(interface, 2)?;
        self.gyro_div.write(interface, range)?;
        Ok(())
    }

    /// Set the rate of sampling of the Gyros
    pub fn set_gyro_lp_factor(
        &self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
        rate: u8,
        // Todo u8 to enum
    ) -> Result<(), ICMError<E>> {
        self.set_bank(interface, 2)?;
        self.gyro_lp_fact.write(interface, rate)?;
        Ok(())
    }

    pub fn init(
        &mut self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
        delay: &mut dyn DelayMs<u16>,
    ) -> Result<(), ICMError<E>> {
        #[cfg(feature = "cortex-m-debuging")]
        hprintln!("start the init ").unwrap();
        self.set_bank(interface, 0)?;
        self.check_chip(interface)?;
        self.restart(interface, delay)?;
        delay.delay_ms(500 as u16);
        self.wake(interface, delay)?;

        self.set_accel_range(interface, delay, AccelRangeOptions::G2)?;
        self.set_accel_div(interface, 125)?;

        self.set_gyro_range(interface, GyroRangeOptions::Deg250)?;
        self.set_gyro_div(interface, 10)?;
        //self.set_gyro_lp_factor(interface, 3)?;

        self.set_bank(interface, 0)?;
        interface.write_register(ICM20X_B0_REG_INT_PIN_CFG, &[0x30])?;

        delay.delay_ms(100 as u16);
        Ok(())
    }

    pub fn set_lp_filter(
        &mut self,
        interface: &mut dyn Interface<Error = InterfaceError<E>>,
        delay: &mut dyn DelayMs<u16>,
    ) -> Result<(), ICMError<E>> {
        self.set_accel_div(interface, 125)?;
        self.set_accel_lp_factor(interface, 1)?;
        self.set_accel_lp_rate(interface, 5)?;
        self.set_accel_lp_enable(interface, 1)?;
        //self.set_accel_lp_dlpfcfg(interface, 2)?;

        // setting low power mode disables most registers
        // so we do it last
        self.set_low_power(interface, true)?;
        Ok(())
    }

    pub fn scale_raw_accel_gyro(
        &self,
        raw: (i16, i16, i16, i16, i16, i16),
    ) -> (f32, f32, f32, f32, f32, f32) {
        let accel_div = self.accel_range_cache.as_float();
        let gyro_div = self.gyro_range_cache.as_float();
        let standard_grav = 9.80665;
        (
            f32::from(raw.0) / accel_div * standard_grav,
            f32::from(raw.1) / accel_div * standard_grav,
            f32::from(raw.2) / accel_div * standard_grav,
            f32::from(raw.3) / gyro_div,
            f32::from(raw.4) / gyro_div,
            f32::from(raw.5) / gyro_div,
        )
    }

    pub fn scale_raw_accel_gyro_bad(
        &self,
        raw: (i16, i16, i16, i16, i16, i16),
    ) -> (f32, f32, f32, f32, f32, f32) {
        let accel_div = self.accel_range_cache.as_float();
        let gyro_div = self.gyro_range_cache.as_float();
        (
            f32::from(raw.0) / gyro_div,
            f32::from(raw.1) / gyro_div,
            f32::from(raw.2) / gyro_div,
            f32::from(raw.3) / accel_div,
            f32::from(raw.4) / accel_div,
            f32::from(raw.5) / accel_div,
        )
    }
}

/// Pre-Prototype implementation for SPI
///
/// Currently this is just here to make sure that the compiler can
/// work with the spi bus, I dont have a chip set up in spi config
/// to test this with but I will set one up soon.
pub struct ICMSPI<SPI, CS, E> {
    phantom: PhantomData<SPI>,
    phantom_e: PhantomData<E>,
    phantom_cs: PhantomData<CS>,
    comm: ICMCommon<dyn Interface<Error = InterfaceError<E>>, E>,
}
impl<SPI, CS, E> ICMSPI<SPI, CS, E>
where
    SPI: Transfer<u8, Error = E>,
    CS: OutputPin,
{
    pub fn new(spi_bus: &mut SPI, cs: &mut CS) -> Result<Self, E> {
        let new = Self {
            phantom: PhantomData,
            phantom_e: PhantomData,
            phantom_cs: PhantomData,
            comm: ICMCommon::new().unwrap(),
        };
        Ok(new)
    }
    pub fn init(
        &mut self,
        spi_bus: &mut SPI,
        cs: &mut CS,
        delay: &mut dyn DelayMs<u16>,
    ) -> Result<(), ICMError<E>> {
        let mut interface = bus::SPIPeripheral::<SPI, CS, E>::new(spi_bus, cs);
        self.comm.init(&mut interface, delay)
    }

    pub fn get_values_accel_gyro(
        &self,
        spi_bus: &mut SPI,
        cs: &mut CS,
    ) -> Result<(i16, i16, i16, i16, i16, i16), ICMError<E>> {
        let mut interface = bus::SPIPeripheral::<SPI, CS, E>::new(spi_bus, cs);
        self.comm.set_bank(&mut interface, 0)?;
        let results = self.comm.result_registers.read(&mut interface)?;

        Ok((
            results[0], results[1], results[2], results[3], results[4], results[5],
        ))
    }

    pub fn scale_raw_accel_gyro(
        &self,
        raw: (i16, i16, i16, i16, i16, i16),
    ) -> (f32, f32, f32, f32, f32, f32) {
        self.comm.scale_raw_accel_gyro(raw)
    }
}

/// Prototype implementation for I2C
///
/// The Accelerometer and Gyro data can be retrieved with this prototype
/// code. I am moving more and more of the code to the ICMCommon code and
/// cleaning it up as I go. Once this is complete I will try it out with
/// SPI too.
pub struct ICMI2C<I2C, E, const ADDRESS: u8> {
    phantom: PhantomData<I2C>,
    phantom_e: PhantomData<E>,
    comm: ICMCommon<dyn Interface<Error = InterfaceError<E>>, E>,
}
impl<I2C, E, const ADDRESS: u8> ICMI2C<I2C, E, ADDRESS>
where
    I2C: Read<Error = E> + Write<Error = E> + WriteRead<Error = E>,
{
    pub fn new(i2c_bus: &mut I2C) -> Result<Self, E> {
        let new = Self {
            phantom: PhantomData,
            phantom_e: PhantomData,
            comm: ICMCommon::new().unwrap(),
        };
        Ok(new)
    }

    pub fn init(
        &mut self,
        i2c_bus: &mut I2C,
        delay: &mut dyn DelayMs<u16>,
    ) -> Result<(), ICMError<E>> {
        let mut interface = bus::I2CPeripheral::<I2C, E, ADDRESS>::new(i2c_bus);
        self.comm.init(&mut interface, delay)?;
        Ok(())
    }

    pub fn set_lp_filter(
        &mut self,
        i2c_bus: &mut I2C,
        delay: &mut dyn DelayMs<u16>,
    ) -> Result<(), ICMError<E>> {
        let mut interface = bus::I2CPeripheral::<I2C, E, ADDRESS>::new(i2c_bus);
        self.comm.set_lp_filter(&mut interface, delay)?;
        Ok(())
    }

    pub fn get_values_accel_gyro(
        &self,
        i2c_bus: &mut I2C,
    ) -> Result<(i16, i16, i16, i16, i16, i16), ICMError<E>> {
        let mut interface = bus::I2CPeripheral::<I2C, E, ADDRESS>::new(i2c_bus);
        let results = self.comm.result_registers.read(&mut interface)?;

        Ok((
            results[0], results[1], results[2], results[3], results[4], results[5],
        ))
    }

    pub fn scale_raw_accel_gyro(
        &self,
        raw: (i16, i16, i16, i16, i16, i16),
    ) -> (f32, f32, f32, f32, f32, f32) {
        self.comm.scale_raw_accel_gyro(raw)
    }

    fn setup_mag(&self, i2c_bus: &mut I2C) -> Result<(), ICMError<E>> {
        {
            let mut interface = bus::I2CPeripheral::<I2C, E, ADDRESS>::new(i2c_bus);
            self.comm.set_bank(&mut interface, 0)?;
        }
        i2c_bus.write(ADDRESS, &[ICM20X_B0_REG_INT_PIN_CFG, 0x30])?;

        {
            let mut interface = bus::I2CPeripheral::<I2C, E, ADDRESS>::new(i2c_bus);
            self.comm.set_bank(&mut interface, 3)?;
        }
        i2c_bus.write(ADDRESS, &[ICM20X_B3_I2C_MST_CTRL, 0x4D])?;
        i2c_bus.write(ADDRESS, &[ICM20X_B3_I2C_MST_DELAY_CTRL, 0x01])?;

        //if not self.mag_read(AK09916_WIA) == AK09916_CHIP_ID:
        //    raise RuntimeError("Unable to find AK09916")
        Ok(())
    }

    fn _set_i2c_bypass(&self, i2c_bus: &mut I2C, bypas: bool) -> Result<(), ICMError<E>> {
        {
            let mut interface = bus::I2CPeripheral::<I2C, E, ADDRESS>::new(i2c_bus);
            self.comm.set_bank(&mut interface, 0)?;
        }
        let mut reg_int: [u8; 1] = [0];
        i2c_bus.write(ICM20948_CHIP_ADR, &[ICM20X_B0_REG_INT_PIN_CFG])?; // , &mut power_buffer).unwrap();
        i2c_bus.read(ICM20948_CHIP_ADR, &mut reg_int)?; // , &mut power_buffer).unwrap();
        #[cfg(feature = "cortex-m-debuging")]
        hprintln!("int pin cfg {:#b}", reg_int[0]).unwrap();
        reg_int[0] = reg_int[0] | ((bypas as u8) << 1);
        //& !0b1;
        #[cfg(feature = "cortex-m-debuging")]
        hprintln!("set to {:#b}", reg_int[0]).unwrap();

        i2c_bus.write(ICM20948_CHIP_ADR, &[ICM20X_B0_REG_INT_PIN_CFG, reg_int[0]])?;
        Ok(())
    }

    fn _set_i2c_set_master(&self, i2c_bus: &mut I2C) -> Result<(), ICMError<E>> {
        {
            let mut interface = bus::I2CPeripheral::<I2C, E, ADDRESS>::new(i2c_bus);
            self.comm.set_bank(&mut interface, 3)?;
        }
        let mut reg_int: [u8; 1] = [0];
        i2c_bus.write(ICM20948_CHIP_ADR, &[ICM20X_B3_I2C_MST_CTRL])?; // , &mut power_buffer).unwrap();
        i2c_bus.read(ICM20948_CHIP_ADR, &mut reg_int)?; // , &mut power_buffer).unwrap();
        #[cfg(feature = "cortex-m-debuging")]
        hprintln!("i2c mst ctrl {:#b}", reg_int[0]).unwrap();
        reg_int[0] = 0x17;
        //& !0b1;
        #[cfg(feature = "cortex-m-debuging")]
        hprintln!("set to {:#b}", reg_int[0]).unwrap();

        i2c_bus.write(ICM20948_CHIP_ADR, &[ICM20X_B3_I2C_MST_CTRL, reg_int[0]])?;
        Ok(())
    }
}

/// Prototype implementation for I2C
///
/// This has a simpler API to make it even easier to use if its the only user of the bus
pub struct ICMI2COwned<I2C, E, const ADDRESS: u8> {
    bus: I2C,
    phantom_e: PhantomData<E>,
    comm: ICMCommon<dyn Interface<Error = InterfaceError<E>>, E>,
}
impl<I2C, E, const ADDRESS: u8> ICMI2COwned<I2C, E, ADDRESS>
where
    I2C: Read<Error = E> + Write<Error = E> + WriteRead<Error = E>,
{
    pub fn new(i2c_bus: I2C) -> Result<Self, E> {
        let new = Self {
            bus: i2c_bus,
            phantom_e: PhantomData,
            comm: ICMCommon::new().unwrap(),
        };
        Ok(new)
    }

    pub fn init(&mut self, delay: &mut dyn DelayMs<u16>) -> Result<(), ICMError<E>> {
        let mut interface = bus::I2CPeripheral::<I2C, E, ADDRESS>::new(&mut self.bus);
        self.comm.init(&mut interface, delay)
    }

    pub fn get_values_accel_gyro(&mut self) -> Result<(i16, i16, i16, i16, i16, i16), ICMError<E>> {
        let mut interface = bus::I2CPeripheral::<I2C, E, ADDRESS>::new(&mut self.bus);
        self.comm.set_bank(&mut interface, 0)?;
        let results = self.comm.result_registers.read(&mut interface)?;

        Ok((
            results[0], results[1], results[2], results[3], results[4], results[5],
        ))
    }

    pub fn scale_raw_accel_gyro(
        &self,
        raw: (i16, i16, i16, i16, i16, i16),
    ) -> (f32, f32, f32, f32, f32, f32) {
        self.comm.scale_raw_accel_gyro(raw)
    }
}
