//! Report the ICM values over usb as serial using the I2C bus
//!
//! This example is for the bluepill, program it and then connect to the usb port.
#![no_std]
#![no_main]
#![feature(destructuring_assignment)]

extern crate cortex_m;
extern crate panic_semihosting;
extern crate stm32f1xx_hal as hal;
extern crate sx127x_lora;

use bme280::spi::BME280;
use cortex_m::asm::delay;
use cortex_m_rt::entry;
use cortex_m_semihosting::*;
use embedded_hal::digital::v2::OutputPin;
use hal::delay::Delay;
use hal::flash::FlashExt;
use hal::gpio::GpioExt;
use hal::prelude::*;
use hal::rcc::RccExt;
use hal::spi::{Mode, Phase, Polarity, Spi};
use hal::time::MegaHertz;
use sx127x_lora::MODE;

const FREQUENCY: i64 = 915;

use arrayvec::ArrayString; // 0.4.10
use core::fmt::Write;

#[entry]
fn main() -> ! {
    let dp = hal::pac::Peripherals::take().unwrap();
    let cp = cortex_m::Peripherals::take().unwrap();

    let mut rcc = dp.RCC.constrain();
    let mut flash = dp.FLASH.constrain();
    let clocks = rcc.cfgr.use_hse(8.mhz()).freeze(&mut flash.acr);
    let mut gpioa = dp.GPIOA.split(&mut rcc.apb2);
    let mut gpiob = dp.GPIOB.split(&mut rcc.apb2);

    let mut reset = gpiob.pb6.into_push_pull_output(&mut gpiob.crl);
    let mut cs_lora = gpiob.pb5.into_push_pull_output(&mut gpiob.crl);
    let mut cs_bme = gpiob.pb8.into_push_pull_output(&mut gpiob.crh);
    cs_lora.set_high();
    cs_bme.set_high();
    reset.set_high();

    let pins = (
        gpiob.pb13.into_alternate_push_pull(&mut gpiob.crh),
        gpiob.pb14.into_floating_input(&mut gpiob.crh),
        gpiob.pb15.into_alternate_push_pull(&mut gpiob.crh),
    ); // (sck, miso, mosi)

    let spi_mode = Mode {
        polarity: Polarity::IdleLow,
        phase: Phase::CaptureOnFirstTransition,
    };
    let mut spi = Spi::spi2(dp.SPI2, pins, spi_mode, 500.khz(), clocks, &mut rcc.apb1);
    let mut buffer = [0; 255];
    let mut arr_message = ArrayString::<[u8; 64]>::new();
    let mut delay_ob = Delay::new(cp.SYST, clocks);
    delay_ob.delay_ms(200_u16);

    let mut icm_obj = ICMSPI::new(&mut spi, &mut cs).unwrap();
    icm_obj.init(&mut spi, &mut cs, &mut delay_obj).unwrap();
    loop {
        delay(clocks.sysclk().0 / 100);

        let bits = icm_obj.get_values_accel_gyro(&mut spi, &mut cs).unwrap();
        hprintln!("{:?}", bits).unwrap();
        let (xa, ya, za, xg, yg, zg) = icm_obj.scale_raw_accel_gyro(bits);

        write!(
            &mut arr_message,
            "results C {:?} {:?} {:?} {:?} {:?} {:?}",
            xa, ya, za, xg, yg, zg
        )
        .expect("Can't write");

        for (i, c) in arr_message.chars().enumerate() {
            buffer[i] = c as u8;
        }

        hprintln!("buffer {:?}?", buffer).unwrap();

        let mut lora = sx127x_lora::LoRa::new(spi, cs_lora, reset, 434, delay_ob).unwrap();
        lora.set_tx_power(17, 1).unwrap(); //Using PA_BOOST. See your board for correct pin.

        let transmit = lora.transmit_payload(buffer, arr_message.len());
        match transmit {
            Ok(()) => hprintln!("Sent packet").unwrap(),
            Err(e) => hprintln!("Error {:?}", e).unwrap(),
        };
        delay(clocks.sysclk().0 * 5);
        (spi, cs_lora, reset, delay_ob) = lora.release();
    }
}
