#![cfg_attr(not(test), no_std)]
use core::fmt::Debug;
use embedded_hal::blocking::delay::DelayMs;
use embedded_hal::blocking::i2c::{Read, Write, WriteRead};
use icm20948::{ICMError, ICM20948_CHIP_ADR, ICMI2C};

/// The MockI2C device really acts as a moc ICM, and has a small amount of state of the ICM
/// to facilitate tests that rely on the responses of the ICM over this Mock bus.
struct MockI2C {
    last_write: Option<u8>,
    chip_id: u8,
}

impl MockI2C {
    fn new() -> Self {
        MockI2C {
            last_write: None,
            chip_id: 0xEA,
        }
    }
    fn custom_id(id: u8) -> Self {
        MockI2C {
            last_write: None,
            chip_id: id,
        }
    }
}

#[derive(Debug)]
enum MockI2CError {
    /// It makes no sense to read from this chip before setting the register you wish to write.
    /// So for now its a error, could be changed if something in the chip spec can be found.
    ReadWithoutWrite,
    /// The driver should only be attempting to communicate with the correct chip.
    ChipNotOnBus,
}

impl Read for MockI2C {
    type Error = MockI2CError;
    /// The Mock holds a last_write state that is used to determine how the mock should reply
    /// we could add more state to make more complex tests.
    fn read(&mut self, address: u8, buffer: &mut [u8]) -> Result<(), Self::Error> {
        if (address == 0x68) | (address == 0x69) {
            Ok(())
        } else {
            Err(MockI2CError::ChipNotOnBus)
        }?;
        match self.last_write {
            None => Err(MockI2CError::ReadWithoutWrite),
            Some(0) => {
                buffer[0] = self.chip_id;
                // Chip ID
                Ok(())
            }
            Some(0x06) => {
                buffer[0] = 0x00;
                // This make the reset code think the chip has reset
                Ok(())
            }
            _ => {
                buffer[0] = 0x55;
                // Fall back result
                Ok(())
            }
        }
    }
}
impl Write for MockI2C {
    type Error = MockI2CError;
    /// All interactions with the icm typically start with the address of the register you wish to read or write two
    /// So we store this first byte as state for the Mock struct so that further reads calls can use this state.
    /// Further down the line we could store more state to make more complex tests
    fn write(&mut self, address: u8, bytes: &[u8]) -> Result<(), Self::Error> {
        if (address == 0x68) | (address == 0x69) {
            Ok(())
        } else {
            Err(MockI2CError::ChipNotOnBus)
        }?;
        self.last_write = Some(bytes[0]);
        Ok(())
    }
}
impl WriteRead for MockI2C {
    type Error = MockI2CError;
    fn write_read(
        &mut self,
        _address: u8,
        _bytes: &[u8],
        _buffer: &mut [u8],
    ) -> Result<(), Self::Error> {
        Ok(())
    }
}

struct MockDelay {}

impl<UXX> DelayMs<UXX> for MockDelay {
    fn delay_ms(&mut self, _ms: UXX) {}
}

#[test]
fn basic_init() {
    let mut i2c = MockI2C::new();
    let mut mock_delay = MockDelay {};
    let mut icm_obj = ICMI2C::<_, _, ICM20948_CHIP_ADR>::new(&mut i2c).unwrap();
    let init_result = icm_obj.init(&mut i2c, &mut mock_delay);
    assert!(matches!(init_result, Ok(())));
}

#[test]
fn bad_init() {
    let mut i2c = MockI2C::custom_id(0xAA);
    let mut mock_delay = MockDelay {};
    let mut icm_obj = ICMI2C::<_, _, ICM20948_CHIP_ADR>::new(&mut i2c).unwrap();
    let init_result = icm_obj.init(&mut i2c, &mut mock_delay);
    assert!(matches!(init_result, Err(ICMError::BadChip)));
}
